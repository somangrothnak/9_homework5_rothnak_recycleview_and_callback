package com.somee.rothnaksomang.homework5_recycleview_with_callback.callback;

import com.somee.rothnaksomang.homework5_recycleview_with_callback.entity.University;

public interface UniversityCallback {
    void delete(University university,int posituion);

    interface AddNewInfo{
        void getUniversity(University university);
    }
    interface UpdateInfo{
        void updateUniversity(University university);
    }
}

